import React, { createContext, useState } from "react";
export const InstagramIdProviderContext = createContext();
export const InstagramIdProviderContextDispatcher = createContext();
const IdContextProvider = ({ children }) => {
  const [instagramID, setInstagramID] = useState("");
  return (
    <InstagramIdProviderContext.Provider value={instagramID}>
      <InstagramIdProviderContextDispatcher.Provider value={setInstagramID}>
        {children}
      </InstagramIdProviderContextDispatcher.Provider>
    </InstagramIdProviderContext.Provider>
  );
};

export default IdContextProvider;
