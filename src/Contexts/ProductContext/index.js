import React, { useState, createContext } from "react";
export const productsProviderContext = createContext();
export const productsProviderContextDispatcher = createContext();
const ProductContextProvider = ({ children }) => {
  let [allProducts, setAllProducts] = useState([]);

  const findCaptionPrice = (text) => {
    let editedText1,
      editedText,
      hashReg,
      regPrice,
      regWord,
      price,
      emojiReg,
      name;
    emojiReg = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g;
    hashReg = /#[ا-ی\w\d\W]+/g;
    editedText1 = text.replace(hashReg, "");
    editedText = editedText1.replace(emojiReg, "");
    regPrice = /قیمت:[ \d]*/g;
    regWord = regPrice.exec(text);
    if (regWord !== null) {
      price = regWord[0].replace("قیمت:", "");
      editedText = editedText.replace(regPrice, "");
    } else {
      price = 0;
    }

    name = editedText.split("");
    name = name.splice(0, 50);
    name = name.join("");

    return {
      caption: editedText,
      price: price,
      name: name,
    };
  };

  const addProducts = (posts) => {
    let editedCaption, captionArray;
    let products = [];

    products = posts.map((item) => {
      captionArray = item.node.edge_media_to_caption.edges;
      if (captionArray.length === 0) {
        return {
          img: item.node.display_url,
          caption: "",
          price: 0,
          ID: item.node.id,
          name: "",
          isChosen: false,
        };
      } else {
        editedCaption = findCaptionPrice(
          item.node.edge_media_to_caption.edges[0].node.text
        );

        return {
          img: item.node.display_url,
          caption: editedCaption.caption,
          price: editedCaption.price,
          ID: item.node.id,
          name: editedCaption.name,
          isChosen: false,
        };
      }
    });
    setAllProducts(products);
  };
  return (
    <productsProviderContext.Provider value={allProducts}>
      <productsProviderContextDispatcher.Provider value={addProducts}>
        {children}
      </productsProviderContextDispatcher.Provider>
    </productsProviderContext.Provider>
  );
};

export default ProductContextProvider;
