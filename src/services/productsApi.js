import api from "./api";

export const getProducts = async (pageAddress) => {
  const { data } = await api.get(pageAddress);
  return data.graphql.user;
};

export default { getProducts };
