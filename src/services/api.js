import axios from "axios";

axios.defaults.baseURL = "https://www.instagram.com/";
axios.interceptors.request.use(
  (request) => {
    return request;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const expectedError =
      error.response &&
      error.response.status >= 400 &&
      error.response.status <= 500;

    if (!expectedError) {
      if ((error + "").includes("Network")) alert("خطا در برقراری ارتباط");
      else alert("خزای غیر منتظره ای رخ داد ");
    } else if (error.response.status === 404) {
      alert("خطا در برقراری ارتباط با سرور");
    } else if (error.response.status === 500) {
      alert("خطا در بررسی درخواست");
    }

    return Promise.reject(error);
  }
);

export default {
  get: axios.get,
  post: axios.post,
  add: axios.add,
  delete: axios.delete,
};
