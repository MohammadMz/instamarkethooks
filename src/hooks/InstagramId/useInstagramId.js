import { useContext } from "react";
import { InstagramIdProviderContext } from "../../Contexts/Idcontext";

function useInstagramId() {
  return useContext(InstagramIdProviderContext);
}

export default useInstagramId;
