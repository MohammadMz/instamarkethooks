import { useContext } from "react";
import { InstagramIdProviderContextDispatcher } from "../../Contexts/Idcontext";

function useInstagramIdActions() {
  return useContext(InstagramIdProviderContextDispatcher);
}

export default useInstagramIdActions;
