import { useContext } from "react";
import { productsProviderContextDispatcher } from "../../Contexts/ProductContext";

function useProductsActions() {
  return useContext(productsProviderContextDispatcher);
}

export default useProductsActions;
