import { useContext } from "react";
import { productsProviderContext } from "../../Contexts/ProductContext/";

function useProducts() {
  return useContext(productsProviderContext);
}

export default useProducts;
