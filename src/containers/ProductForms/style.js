import { makeStyles } from "@material-ui/core";

const styles = makeStyles((theme) => ({
  box: {
    padding: theme.spacing(1),
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
    alignItems: "center",
    justifyContent: "center",
    width: "auto",
    transition: "all 0.3s ease-out",
    [theme.breakpoints.up("md")]: {
      maxHeight: "500px",
    },
  },
  textArea: {
    width: "90%",
    borderRadius: "5px",
    marginTop: theme.spacing(1),
  },
  input: {
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
    width: "90%",
    borderColor: "blue",
  },
  inputGrid: {
    alignSelf: "stretch",
    marginTop: theme.spacing(3),
  },
  thumbnail: {
    maxWidth: "250px",
    maxHeight: "300px",
    transition: "all .3s ease-out",
    opacity: ".9",
    "&:hover": {
      transform: "scale(1.2)",
      opacity: "1",
    },
  },
  imgGrid: {
    textAlign: "center",
  },
  label: {
    paddingTop: "10px",
  },
}));

export default styles;
