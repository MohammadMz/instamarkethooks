import React, { useState, useEffect } from "react";
import {
  Typography,
  TextareaAutosize,
  Checkbox,
  TextField,
  Grid,
  Input,
} from "@material-ui/core";
import styles from "./style";
import { Skeleton } from "@material-ui/lab";

const ProductForms = (props) => {
  const [loadSkeleton, setLoadSkeleton] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setLoadSkeleton(true);
    }, 2000);
  }, []);

  const classes = styles();

  return (
    <form>
      <Grid container my={4} className={classes.box}>
        <Grid item xs={2} md={1}>
          {loadSkeleton ? (
            <Checkbox checked={props.product.ischosen} color="primary" />
          ) : (
            <Skeleton variant="rect">
              <Checkbox />
            </Skeleton>
          )}
        </Grid>

        <Grid item xs={10} md={4} className={classes.imgGrid}>
          {loadSkeleton ? (
            <img
              src={props.product.img}
              alt="test-img"
              className={classes.thumbnail}
            />
          ) : (
            <Skeleton
              variant="rect"
              width={250}
              height={250}
              className={classes.imgGrid}
            />
          )}
        </Grid>
        <Grid item xs={12} md={7} container className={classes.inputGrid}>
          <Grid item xs={4} md={2} className={classes.label}>
            {loadSkeleton ? (
              <Typography component="label">نام کالا:</Typography>
            ) : (
              <Skeleton variant="text">
                <Typography />
              </Skeleton>
            )}
          </Grid>
          <Grid xs={8} md={10} item>
            {loadSkeleton ? (
              <TextField
                required
                id="productName"
                name="productname"
                fullWidth
                defaultValue={props.product.name}
                variant="outlined"
                size="small"
                className={classes.input}
              ></TextField>
            ) : (
              <Skeleton variant="text">
                <TextField fullWidth size="small" className={classes.input} />
              </Skeleton>
            )}
          </Grid>
          <Grid item xs={4} md={2} className={classes.label}>
            {loadSkeleton ? (
              <Typography component="label">قیمت:</Typography>
            ) : (
              <Skeleton variant="text">
                <Typography component="label" />
              </Skeleton>
            )}
          </Grid>
          <Grid item xs={8} md={10}>
            {loadSkeleton ? (
              <TextField
                id="price"
                name="price"
                type="number"
                defaultValue={props.product.price}
                variant="outlined"
                size="small"
                className={classes.input}
              ></TextField>
            ) : (
              <Skeleton variant="text">
                <TextField size="small" className={classes.input} />
              </Skeleton>
            )}
          </Grid>
          <Grid item xs={4} md={2} className={classes.label}>
            {loadSkeleton ? (
              <Typography component="label">توضیحات:</Typography>
            ) : (
              <Skeleton variant="text">
                <Typography component="label" />
              </Skeleton>
            )}
          </Grid>
          <Grid item xs={8} md={10}>
            {loadSkeleton ? (
              <TextareaAutosize
                id="details"
                name="details"
                defaultValue={props.product.caption}
                className={classes.textArea}
                rowsMin={3}
                rowsMax={10}
              ></TextareaAutosize>
            ) : (
              <Skeleton variant="text">
                <TextareaAutosize className={classes.textArea} row={3} />
              </Skeleton>
            )}
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

export default ProductForms;
