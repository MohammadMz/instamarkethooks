import React, { useEffect, useState } from "react";
import productService from "../../services/productsApi";
import styles from "./style";
import ProductForms from "../ProductForms";
import {
  Grid,
  CircularProgress,
  Divider,
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableBody,
} from "@material-ui/core";
import useProducts from "../../hooks/Products/useProducts";
import useProductsActions from "../../hooks/Products/useProductsActions";
import useInstagramId from "../../hooks/InstagramId/useInstagramId";
import useInstagramIdActions from "../../hooks/InstagramId/useInstagramIdActions";

const Data = () => {
  const classes = styles();
  const allProducts = useProducts();
  const setAllProductsContext = useProductsActions();
  const instagramId = useInstagramId();
  const [dialog, setDialog] = useState(false);
  const [laoding, setLoading] = useState(true);
  const [url, setUrl] = useState("");
  const [errorStatus, setErrorStatus] = useState("صفحه مورد نظر موجود نیست.");

  useEffect(() => {
    setLoading(true);
    if (instagramId) {
      loadPost();
      setLoading(true);
    }
  }, [instagramId]);

  const loadPost = () => {
    //generating the url from instagram id that has been taken from the user in the shop component

    let pageAddress = `${instagramId}/?__a=1`;

    productService
      .getProducts(pageAddress)
      .then((result) => {
        setLoading(false);
        let user = result;
        let allPosts = user.edge_owner_to_timeline_media.edges;
        let pageInfo = user.edge_owner_to_timeline_media.page_info;
        let paginationData = {
          hasNextPage: pageInfo.has_next_page,
          endCursor: pageInfo.end_cursor,
        };
        let nextPage = encodeURIComponent(
          `{"id:"'${user.id}'","first":50,"after":"'${paginationData.endCursor}'"}`
        );
        let url = "https://www.instagram.com/graphql/query/";
        let finalUrl =
          url +
          "?query_hash=472f257a40c653c64c666ce877d59d2b&query_id=17888483320059182&variables=" +
          nextPage;
        setUrl(finalUrl);
        console.log(finalUrl);
        setAllProductsContext(allPosts);
      })
      .catch((er) => {
        console.log(er);
        setLoading(false);
        setDialog(true);
      });
  };

  let forms = null;

  if (allProducts.length !== 0) {
    forms = allProducts.map((item) => {
      return (
        <Box key={item.ID}>
          <ProductForms product={item} />
          <Divider variant="middle" className={classes.divider} />
        </Box>
      );
    });
  }

  return (
    <Box>
      {laoding ? (
        <Box className={classes.loader}>
          <CircularProgress />
        </Box>
      ) : null}

      <Dialog
        open={dialog}
        onClose={() => setDialog(false)}
        className={classes.dialog}
      >
        <DialogTitle>خطا</DialogTitle>
        <DialogContent>
          <DialogContentText>{errorStatus}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={() => setDialog(false)}
          >
            بستن{" "}
          </Button>
        </DialogActions>
      </Dialog>

      {forms ? (
        <Grid>
          {forms}
          <Button
            variant="contained"
            color="primary"
            size="small"
            className={classes.button}
          >
            افزودن محصولات{" "}
          </Button>
        </Grid>
      ) : null}
    </Box>
  );
};

export default Data;
