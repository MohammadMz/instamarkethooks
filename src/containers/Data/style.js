import { makeStyles } from "@material-ui/core";

const styles = makeStyles((theme) => ({
  divider: {
    backgroundColor: "blue",
  },
  button: {
    float: "right",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    fontSize: "14px",
  },
  loader: {
    margin: "60px auto",
    textAlign: "center",
  },
  dialog: {
    minWidth: "400px",
  },
  dialogheader: {
    color: "red",
  },
}));

export default styles;
