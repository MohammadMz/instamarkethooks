import { makeStyles } from "@material-ui/core";

const styles = makeStyles({
  box: {
    position: "relative",
    // backgroundImage:
    //   "url('../../../../assets/Facebook-Marketplace.jpg'),linear-gradient(to right bottom,rgba(60,59,65,0.89),rgba(19,13,109,0.8))",
    height: "60vh",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    color: "#fff",
  },
});

export default styles;
