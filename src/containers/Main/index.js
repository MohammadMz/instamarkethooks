import React from "react";
import { Typography, Grid } from "@material-ui/core";
import styles from "./style";

const Main = () => {
  const classes = styles();
  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      className={classes.box}
    >
      <Grid item xs={6} md={4}>
        <Typography align="center" variant="h4" gutterBottom>
          فروشگاه ساز سپهر، راهکار یکپارچه فروش برای فروش اینترنتی
        </Typography>
        <Typography align="center" variant="h5" gutterBottom>
          دیگه فقط وب سایت کافی نیست!
        </Typography>
      </Grid>
    </Grid>
  );
};

export default Main;
