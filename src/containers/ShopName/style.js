import { makeStyles } from "@material-ui/core";
import theme from "../../Theme";

const styles = makeStyles((theme) => ({
  root: {
    marginTop: "60px",
  },
  button: {
    float: "right",
    [theme.breakpoints.up("sm")]: {
      float: "left",
    },
  },
}));

export default styles;
