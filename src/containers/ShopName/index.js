import React, { useState } from "react";
import useInstagramId from "../../hooks/InstagramId/useInstagramId";
import useInstagramIdActions from "../../hooks/InstagramId/useInstagramIdActions";
import styles from "./style";
import Data from "../Data";
import {
  Button,
  Typography,
  Container,
  Grid,
  TextField,
} from "@material-ui/core/";

const ShopName = () => {
  const instagramID = useInstagramId();
  const setInstagramIdContext = useInstagramIdActions();
  let [input, setInput] = useState("");

  const classes = styles();

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      setInstagramIdContext(input);
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <Container maxWidth="lg" className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={3} md={2}>
          <Typography variant="h6">آی دی اینستاگرام:</Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={8}>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <TextField
              size="small"
              id="shopName"
              required
              variant="outlined"
              fullWidth
              autoFocus
              onChange={(e) => setInput(e.target.value)}
              onKeyPress={handleKeyPress}
            />
          </form>
        </Grid>
        <Grid item xs={12} sm={3} md={2}>
          <Button
            onClick={() => setInstagramIdContext(input)}
            color="primary"
            variant="contained"
            className={classes.button}
          >
            ثبت فروشگاه
          </Button>
        </Grid>
      </Grid>
      {instagramID ? <Data /> : null}
    </Container>
  );
};

export default ShopName;
