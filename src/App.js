import React from "react";
import "./App.css";
import Main from "./containers/Main";
import ShopName from "./containers/ShopName";
import { ThemeProvider } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core/";
import theme from "./Theme/";
import IdContextProvider from "./Contexts/Idcontext";
import ProductContextProvider from "./Contexts/ProductContext";
import RTL from "./RTl";
function App() {
  return (
    <div>
      <React.Fragment>
        <IdContextProvider>
          <ProductContextProvider>
            <ThemeProvider theme={theme}>
              <RTL>
                <Paper elevation={0}>
                  <Main />
                  <ShopName />
                </Paper>
              </RTL>
            </ThemeProvider>
          </ProductContextProvider>
        </IdContextProvider>
      </React.Fragment>
    </div>
  );
}

export default App;
